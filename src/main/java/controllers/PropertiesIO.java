package controllers;

import POJO.Question;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class PropertiesIO {

    public void saveQuestion(Question question) throws IOException {
        java.util.Properties properties = new java.util.Properties();
        FileWriter fileWriter = new FileWriter("question.PropertiesIO");
        properties.setProperty("operation", question.getOperationType());
        properties.setProperty("firstIngredient", String.valueOf(question.getFirstIgredient()));
        properties.setProperty("secondIngredient", String.valueOf(question.getSecondIgredient()));
        properties.setProperty("correctAnswer", String.valueOf(question.getCorrectAnswer()));
        properties.setProperty("uncorrectAnswerA", String.valueOf(question.getUncorrectAnswerA()));
        properties.setProperty("uncorrectAnswerB", String.valueOf(question.getUncorrectAnswerB()));
        properties.store(fileWriter,"");

    }

    public Question loadQuestion() throws IOException {
        java.util.Properties properties = new java.util.Properties();
        Question question = new Question();
        FileReader fileReader = new FileReader("question.PropertiesIO");
        properties.load(fileReader);
        question.setOperationType(properties.getProperty("operation"));
        question.setFirstIgredient(Integer.parseInt(properties.getProperty("firstIngredient")));
        question.setSecondIgredient(Integer.parseInt(properties.getProperty("secondIngredient")));
        question.setCorrectAnswer(Integer.parseInt(properties.getProperty("correctAnswer")));
        question.setUncorrectAnswerA(Integer.parseInt(properties.getProperty("uncorrectAnswerA")));
        question.setUncorrectAnswerB(Integer.parseInt(properties.getProperty("uncorrectAnswerB")));

        return question;

    }
}
